package model.logic;

public class NodeRedVial 
{
	private int idNode;
	
	private float longitud;
	
	private float latitud;
	
	public NodeRedVial(int id, float pLongitud, float pLatitud) 
	{
		idNode = id;
		longitud = pLongitud;
		latitud = pLatitud;
	}
	
	public int darId()
	{
		return idNode;
	}
	
	public float darLongitud()
	{
		return longitud;
	}
	
	public float darLatitud()
	{
		return latitud;
	}
}
