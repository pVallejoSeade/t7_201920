package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.google.gson.JsonParser;
import com.google.gson.annotations.Expose;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.opencsv.CSVReader;
import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.LatLng;

import java.io.IOException;

import model.data_structures.GrafoNoDirigido;
import model.data_structures.Iterator;
import model.data_structures.LinearProbingHashT;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;
import model.data_structures.Vertex;

import java.lang.Comparable;

import java.lang.Math;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import org.junit.runner.manipulation.Sorter;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{

	@Expose private GrafoNoDirigido<Integer,Interseccion> graph;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		graph= new GrafoNoDirigido<Integer, Interseccion>(0);
	}

	public void loadGraph()
	{

		CSVReader reader = null;

		try {
			reader = new CSVReader(new FileReader("./data/bogota_vertices.txt"));
			reader.readNext();
			int vert=0;
			for(String[] nextLine : reader) {
				for(String st: nextLine)
				{
					String[] linea=st.split(";");
					int a = Integer.parseInt(linea[0]);//id
					double b = Float.parseFloat(linea[1]);//long
					double c = Float.parseFloat(linea[2]);//lat
					int d = Integer.parseInt(linea[3]);//zona
					
					Interseccion i= new Interseccion(a,b,c,d);
					vert++;
					graph.addVertex(a, i);
				}
				
			}
			System.out.println("Vertices: " + vert);


			reader = new CSVReader(new FileReader("./data/bogota_arcos.txt"));
			int arc=0;
			for(String[] nextLine : reader) {
				for(String st: nextLine)
				{
					String[] linea=st.split(" ");
					
					int id = Integer.parseInt(linea[0]);//id
					for(int i=1;i<linea.length && graph.darVertices().get(id)!=null;i++)//Veo los vecinos
					{
						
						int v = Integer.parseInt(linea[i]);
						if(graph.darVertices().get(v)!=null)
						{
							//Haversine calculation
							double lo1= graph.getInfoVertex(id).darLongitud();
							double lo2=graph.getInfoVertex(v).darLongitud();
							double la1=graph.getInfoVertex(id).darLatitud();
							double la2=graph.getInfoVertex(v).darLatitud();
							Haversine calc= new Haversine();
							double val=calc.distance(la1, lo1, la2, lo2);
							double cost=calc.haversin(val);
							
							graph.addEdge(id, v, cost);
							arc++;
						}					
						
					}
				}
				
			}
			System.out.println("Arcos: " + arc);
			System.out.println();


		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public GrafoNoDirigido<Integer,Interseccion> darGrafo()
	{
		return graph;
	}
	
	public void persistirGrafo()
	{
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		graph.verticesArreglo();
		String jsonString = gson.toJson(graph);//Falta que guarde el grafo, cosas como lo anterior s� lo guarda

	    System.out.println("JSON: " + jsonString);
	    try {
	    	String filename = "./data/grafo_persistido1.json";
	    	FileWriter file = new FileWriter(filename);
			file.write(jsonString);
			file.flush();
			file.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	}
	
	public GrafoNoDirigido<Integer, Interseccion> cargarGrafoPersistido()
	{
		GrafoNoDirigido<Integer, Interseccion> grafo= new GrafoNoDirigido<Integer, Interseccion>(0);
		
		int vertex=0;
		int arcos=0;
		
		String filename = "./data/grafo_persistido.json";
		try {
			JsonReader json = new JsonReader(new FileReader(filename));
			
			JsonObject o = new JsonParser().parse(json).getAsJsonObject();
			JsonArray vertices = o.get("vertex").getAsJsonArray();
			
			for(JsonElement element:vertices)
			{
				JsonObject objeto = element.getAsJsonObject();
				int id=objeto.get("idVertex").getAsInt();
				JsonObject intersection = objeto.get("infoVertex").getAsJsonObject();
				double longi=intersection.get("longitud").getAsDouble();
				double lat=intersection.get("latitud").getAsDouble();
				int zona=intersection.get("zonaUber").getAsInt();
				Interseccion i= new Interseccion(id, longi, lat, zona);
				
				grafo.addVertex(id, i);
				vertex++;
				
				JsonArray vecinos=objeto.get("listaAdj").getAsJsonArray();
				for(JsonElement elem: vecinos)
				{
					JsonObject object = elem.getAsJsonObject();
					int idVecino=object.get("idVecino").getAsInt();
					if(grafo.darVertices().get(idVecino)!=null)
					{
						double costo=object.get("costo").getAsDouble();
						
						grafo.addEdge(id, idVecino, costo);
						arcos++;
					}					
				}			
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Vertices: "+vertex);
		System.out.println("Arcos: "+arcos);
		
		return grafo;
	}

	public void pintarMapa(double longMin, double longMax, double latMin, double latMax)
	{
		
		Mapa map= new Mapa();
		RedBlackBST<Double, Vertex<Integer, Interseccion>> arbol= new RedBlackBST<Double, Vertex<Integer,Interseccion>>();		
		
		Iterator it=graph.darVertices().keys();
		while(it.hasNext())
		{
			Vertex<Integer, Interseccion> v= (Vertex<Integer, Interseccion>)it.next();
			arbol.put(v.darInfo().darLongitud(), v);
		}
		
		RedBlackBST<Double, Vertex<Integer, Interseccion>> arbol2= new RedBlackBST<Double, Vertex<Integer,Interseccion>>();	
		
		it=arbol.valuesOfKeysInRange(longMin, longMax);		
		while(it.hasNext())
		{
			Vertex<Integer, Interseccion> v= (Vertex<Integer, Interseccion>)it.next();
			arbol2.put(v.darInfo().darLatitud(), v);
		}
		
		ArrayList<Vertex<Integer, Interseccion>> verticesRango= new ArrayList<Vertex<Integer,Interseccion>>();
		
		it=arbol2.valuesOfKeysInRange(latMin, latMax);
		while(it.hasNext())
		{
			Vertex<Integer, Interseccion> v= (Vertex<Integer, Interseccion>)it.next();
			verticesRango.add(v);
			
			map.agregarVertice((new LatLng(v.darInfo().darLatitud(), v.darInfo().darLongitud())));
			//Pintar vertice
			
		}
		
		for(Vertex<Integer, Interseccion> v: verticesRango) {
			Interseccion in1=v.darInfo();
			ArrayList<Vertex<Integer, Interseccion>.Node<Integer>>  vecinos= v.darListaAdj();
			for(Vertex<Integer, Interseccion>.Node<Integer> ve: vecinos)
			{
				if(verticesRango.contains(ve.darElemento()))
				{
					//Pintar arco
					Interseccion in=ve.darElemento().darInfo();
					map.agregarArco(in1.darLatitud(), in1.darLongitud(), in.darLatitud(), in.darLongitud());
				}
			}
		}
		map.show(map);
		
	}
}
