package model.logic;

import com.google.gson.annotations.Expose;

public class Interseccion {

	private int idNode;

	@Expose private double longitud;

	@Expose private double latitud;
	
	@Expose private int zonaUber;

	public Interseccion(int id, double pLongitud, double pLatitud, int pZonaUber) 
	{
		idNode = id;
		longitud = pLongitud;
		latitud = pLatitud;
		zonaUber=pZonaUber;
	}

	public int darId()
	{
		return idNode;
	}

	public double darLongitud()
	{
		return longitud;
	}

	public double darLatitud()
	{
		return latitud;
	}
	
	public int darZona()
	{
		return zonaUber;
	}

}
