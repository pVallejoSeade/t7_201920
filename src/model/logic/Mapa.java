package model.logic;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapMouseEvent;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.MouseEvent;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.swing.MapView;

import javax.swing.*;
import java.awt.*;

public class Mapa extends MapView{

	Map mapa;
	public Mapa() {

		// Setting of a ready handler to MapView object. onMapReady will be called when map initialization is done and
		// the map object is ready to use. Current implementation of onMapReady customizes the map object.
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				// Check if the map is loaded correctly
				if (status == MapStatus.MAP_STATUS_OK) {
					// Getting the associated map object
					final Map map = getMap();
					// Creating a map options object
					MapOptions mapOptions = new MapOptions();
					// Creating a map type control options object
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					// Changing position of the map type control
					controlOptions.setPosition(ControlPosition.TOP_RIGHT);
					// Setting map type control options
					mapOptions.setMapTypeControlOptions(controlOptions);
					// Setting map options
					map.setOptions(mapOptions);
					// Setting the map center
					map.setCenter(new LatLng(4.582989396000016, -74.08921298299998));
					// Setting initial zoom value
					map.setZoom(3.0);

					mapa=map;
				}
			}
		});
	}

	public void agregarArco(double latIni, double longIni, double latFin, double longFin)
	{
		LatLng[] path = {new LatLng(latIni, longIni),
				new LatLng(latFin, longFin)};
		// Creating a new polyline object
		Polyline polyline = new Polyline(mapa);
		// Initializing the polyline with created path
		polyline.setPath(path);
	}

	public void agregarVertice(LatLng p)
	{
		Circle punto= new Circle(mapa);
		punto.setCenter(p);
	}

	public void show(Mapa m)
	{
		JFrame frame = new JFrame("Map");

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(m, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
