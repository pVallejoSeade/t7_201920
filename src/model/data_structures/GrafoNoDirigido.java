package model.data_structures;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.annotations.Expose; 

public class GrafoNoDirigido <K extends Comparable<K>,V>
{

	private int numVertices;
	private int numEdges;
	private RedBlackBST<K, Vertex<K, V>> vertices;
	@Expose private ArrayList<Vertex<K, V>> vertex;

	
	
	public GrafoNoDirigido(int v)
	{
		numVertices = v;
		numEdges = 0;
		vertices = new RedBlackBST<>();
	}
	
	public int V()
	{
		return numVertices;
	}
	
	public int E()
	{
		return numEdges;
	}
	
	public RedBlackBST<K, Vertex<K, V>> darVertices()
	{
		return vertices;
	}
	
	public void verticesArreglo()
	{
		ArrayList<Vertex<K, V>> arr= new ArrayList<Vertex<K,V>>();
		model.data_structures.Iterator<K> keys=  vertices.keys();
		while(keys.hasNext())
		{
			K k=keys.next();
			Vertex< K, V> v=vertices.get(k);
			arr.add(v);
		}
		vertex=arr;
	}
	
	public void addEdge(K idVertexIni, K idVertexFin, double cost)
	{
		numEdges ++;
		vertices.get(idVertexIni).agregarAListaAdj(idVertexFin, cost,vertices.get(idVertexFin));
		vertices.get(idVertexFin).agregarAListaAdj(idVertexIni, cost, vertices.get(idVertexIni));

	}
	
	public void addVertex(K idVertex, V infoVertex)
	{
		Vertex<K, V> v = new Vertex<K, V>(idVertex, infoVertex);
		vertices.put(idVertex, v);
		numVertices ++;
	}
	
	public V getInfoVertex(K idVertex)
	{
		return vertices.get(idVertex).darInfo();
	}
	
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		vertices.get(idVertex).setInfoVertex(infoVertex);
	}
	
	public double getCostArc(K idVertexIni,K idVertexFin)
	{
		return vertices.get(idVertexIni).getCostArc(idVertexFin);
	}
	
	public void setCostArc(K idVertexIni, K idVertexFin, double cost)
	{
		vertices.get(idVertexIni).setCost(idVertexFin, cost);
		vertices.get(idVertexFin).setCost(idVertexIni, cost);
	}

	public Iterator<K> adj (K idVertex) 
	{
		Iterator<Vertex<K,V>.Node<K>> iter = vertices.get(idVertex).darListaAdj().iterator();
		ArrayList<K> lista = new ArrayList<>();
		while(iter.hasNext())
		{
			Vertex<K,V>.Node<K> nodo = iter.next();
			lista.add(nodo.darId());
		}
		
		if (lista.isEmpty())
		{
			return null;
		}
		return lista.iterator();
	}
	
	public void uncheck()
	{
		model.data_structures.Iterator<K> iter =  vertices.keys();
		while (iter.hasNext())
		{
			K key = iter.next();
			vertices.get(key).uncheck();
		}
	}
	
	public void dfs(K s)
	{
		vertices.get(s).dfs();
		
	}
	
	public int cc()
	{
		uncheck();
		model.data_structures.Iterator<K> iter = vertices.keys();
		int resultado = 0;
		while (iter.hasNext())
		{
			Vertex<K, V> v = vertices.get(iter.next());
			if (!v.isMarked())
			{
				dfs(v.darId());
				resultado ++;
			}
		}
		
		return resultado;
	}
	
	public model.data_structures.Iterator<K> getCC(K idVertex)
	{
		uncheck();
		dfs(idVertex);
		LinkedList<K> lista = new LinkedList<>();
		model.data_structures.Iterator<K> iter = vertices.keys();
		while (iter.hasNext())
		{
			Vertex<K, V> v = vertices.get(iter.next());
			if (v.isMarked())
			{
				lista.append(v.darId());
			}
		}
		
		return lista.iterator();
	}
	
}
