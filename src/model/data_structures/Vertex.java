package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.annotations.Expose;

public class Vertex<K, V> 
{
	@Expose private K idVertex;
	@Expose private V infoVertex;
	@Expose private ArrayList<Node<K>> listaAdj;
	private boolean marked;
	
	public class Node <E>
	{
		@Expose private E idVecino;
		@Expose private double costo;
		private Vertex<E, V> asociado;
		
		public Node(E elem, double cost , Vertex<E, V> v)
		{
			idVecino = elem;
			costo = cost;
			//marked =false;
			asociado=v;
			
		}
		
		public Vertex<E, V> darElemento(){
			return asociado;
		}
		
		public E darId()
		{
			return idVecino;
		}
		
		
		public double darCosto()
		{
			return costo;
		}
		
		public void cambiarCosto(double pCost)
		{
			costo = pCost;
		}
	}
	
	public Vertex(K id, V info)
	{
		idVertex = id;
		infoVertex = info;
		listaAdj = new ArrayList<>();
	}
	
	public K darId()
	{
		return idVertex;
	}
	
	public V darInfo()
	{
		return infoVertex;
	}
	
	public ArrayList<Node<K>> darListaAdj()
	{
		return listaAdj;
	}
	
	public double getCostArc(K idVertice)
	{
		Iterator<Vertex<K,V>.Node<K>> iter = listaAdj.iterator();
		while(iter.hasNext())
		{
			Vertex<K,V>.Node<K> nodo = iter.next();
			if (nodo.darId() == idVertice)
			{
				return nodo.darCosto();
			}
		}
		
		return -1;
	}
	
	public void setCost(K id, double cost)
	{
		Iterator<Vertex<K,V>.Node<K>> iter = listaAdj.iterator();
		while(iter.hasNext())
		{
			Vertex<K,V>.Node<K> nodo = iter.next();
			if (nodo.darId() == id)
			{
				nodo.cambiarCosto(cost);
			}
		}
	}
	
	public void setInfoVertex(V info)
	{
		infoVertex = info;
	}
	
	public void agregarAListaAdj(K adjVertex, double cost, Vertex<K, V> v)
	{
		Node<K> nodo = new Node<K>(adjVertex, cost,v);
		listaAdj.add(nodo);
	}
	
	public void check()
	{
		marked = true;
	}
	
	public void uncheck()
	{
		marked = false;
	}
	
	public boolean isMarked()
	{
		if(marked==true)
		{
			return true;
		}
		return false;
	}
	
	public void dfs()
	{
		check();
		Iterator<Vertex<K, V>.Node<K>> iter = listaAdj.iterator();
		while (iter.hasNext())
		{
			Node<K> nodo = (Node<K>) iter.next();
			if (!nodo.darElemento().isMarked())
			{
				nodo.darElemento().dfs();
			}
		}
	}
}
