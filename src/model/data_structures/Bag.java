package model.data_structures;

public class Bag<E> implements Iterable<E>
{
	private Node<E> primero;
	private int tamanio;
	
	public class Node <E>
	{
		private E elemento;
		private Node<E> siguiente;
		private double costo;
		
		public E darElemento()
		{
			return elemento;
		}
		
		public Node<E> darSiguiente()
		{
			return siguiente;
		}
		
		public double darCosto()
		{
			return costo;
		}
		
		public void cambiarCosto(double pCost)
		{
			costo = pCost;
		}
	}
	
	public Bag()
	{
		primero = null;
		tamanio = 0;
	}
	
	public boolean isEmpty()
	{
		return primero == null;
	}
	
	public int size()
	{
		return tamanio;
	}
	
	public void add(E elem, double cost)
	{
		Node<E> primeroViejo = primero;
		primero = new Node<E>();
		primero.elemento = elem;
		primero.costo = cost;
		primero.siguiente = primeroViejo;
		tamanio ++;
	}
	
	public Iterator<E> iterator()
	{
		return new Iterator<E>() 
		{
			Node <E> act = null;
			public boolean hasNext()
			{
				if (tamanio == 0)
				{
					return false;
				}	
				if (act == null)
				{
					return true;
				}
				return act.siguiente != null;
			}
			public E next()
			{
				if (act == null)
				{
					act = primero;
				}	
				else
				{
					act = act.siguiente;
				}
				return act.elemento;
			}
		};
	}
}
