package controller;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.GrafoNoDirigido;
import model.data_structures.Iterator;
import model.data_structures.LinkedList;
import model.logic.Interseccion;
import model.logic.MVCModelo;
import model.logic.NodeRedVial;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
//		String dato = "";
//		String respuesta = "";
//		int i;
//		TravelTime v;
//		int numZonas;
		
		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar viajes: ");
				    //int capacidad = lector.nextInt();
				    modelo = new MVCModelo(); 
				    modelo.loadGraph();
					System.out.println("Datos cargados");
					System.out.println("---------");						
					break;
					
				case 2:
					System.out.println("--------- \nGenerar JSON: ");
					modelo.persistirGrafo();
					System.out.println("---------");
					break;
					
				case 3:
					System.out.println("--------- \nCargar grafo persistido: ");
					GrafoNoDirigido<Integer, Interseccion> grafo=modelo.cargarGrafoPersistido();
					System.out.println("\n---------");
					break;
					
				case 4:
					System.out.println("--------- \nDar cantidad de componentes conectados: ");
					System.out.println("CC: "+modelo.darGrafo().cc());
					
					System.out.println("---------");
					break;
					
//				case 5: 
//					System.out.println("--------- \nBuscar los N zonas que están más al norte:");
//					System.out.println("Ingresar número de viajes que se desea consultar:");
//					numZonas = lector.nextInt();
//					modelo.req1B(numZonas);
//					System.out.println("---------");
//					break;	

				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
