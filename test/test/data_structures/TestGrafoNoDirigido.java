package test.data_structures;

import junit.framework.TestCase;

import model.data_structures.GrafoNoDirigido;
import model.data_structures.Iterator;
import model.data_structures.LinearProbingHashT;
import model.data_structures.RedBlackBST;
import model.data_structures.Vertex;

public class TestGrafoNoDirigido extends TestCase
{
	public void testAddVertexYV()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		assertEquals(1, grafo.V());
		
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		assertEquals(4, grafo.V());
	}
	
	public void testGetInfoVertez()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		
		assertEquals("a", grafo.getInfoVertex(0));
		assertEquals("b", grafo.getInfoVertex(1));
		assertEquals("c", grafo.getInfoVertex(2));
		assertEquals("d", grafo.getInfoVertex(3));
		
		grafo = null;
	}
	
	public void testSetInfoVertez()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		
		grafo.setInfoVertex(0, "aa");
		assertEquals("aa", grafo.getInfoVertex(0));
		
		grafo.setInfoVertex(1, "bb");
		grafo.setInfoVertex(2, "cc");
		grafo.setInfoVertex(3, "dd");
		assertEquals("bb", grafo.getInfoVertex(1));
		assertEquals("cc", grafo.getInfoVertex(2));
		assertEquals("dd", grafo.getInfoVertex(3));
		
		grafo = null;
	}
	
	public void testEYAddEdge()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		
		grafo.addEdge(0, 1, 0.1);
		assertEquals(1, grafo.E());
		
		grafo.addEdge(1, 2, 1.2);
		grafo.addEdge(2, 3, 3.4);
		assertEquals(3, grafo.E());
		
		grafo = null;
	}
	
	public void testGetCostEdge()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		
		grafo.addEdge(0, 1, 0.1);
		grafo.addEdge(1, 2, 1.2);
		grafo.addEdge(2, 3, 2.3);
		
		assertEquals(0.1, grafo.getCostArc(0, 1));
		assertEquals(0.1, grafo.getCostArc(1, 0));
		assertEquals(1.2, grafo.getCostArc(1, 2));
		assertEquals(1.2, grafo.getCostArc(2, 1));
		assertEquals(2.3, grafo.getCostArc(2, 3));
		assertEquals(2.3, grafo.getCostArc(3, 2));
		
		grafo = null;
	}
	
	public void setCostArc()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		
		grafo.addEdge(0, 1, 0.1);
		grafo.addEdge(1, 2, 1.2);
		grafo.addEdge(2, 3, 2.3);
		
		grafo.setCostArc(0, 1, 1.0);
		assertEquals(1.0, grafo.getCostArc(0, 1));
		assertEquals(1.0, grafo.getCostArc(1, 0));
		
		grafo.setCostArc(1, 2, 2.1);
		grafo.setCostArc(2, 3, 3.2);
		assertEquals(2.1, grafo.getCostArc(1, 2));
		assertEquals(2.1, grafo.getCostArc(2, 1));
		assertEquals(3.2, grafo.getCostArc(2, 3));
		assertEquals(3.2, grafo.getCostArc(3, 2));
		
		grafo = null;
	}
	
	public void testAdj()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		grafo.addVertex(4, "e");
		grafo.addVertex(5, "f");
		grafo.addVertex(6, "g");
		grafo.addVertex(7, "h");
		grafo.addVertex(8, "i");
		grafo.addVertex(9, "j");
		grafo.addVertex(10, "k");
		grafo.addVertex(11, "l");
		grafo.addVertex(12, "m");
		grafo.addVertex(13, "n");
		grafo.addVertex(14, "o");
		grafo.addVertex(15, "p");
		grafo.addVertex(16, "q");
		grafo.addVertex(17, "r");
		grafo.addVertex(18, "s");
		grafo.addVertex(19, "t");
		grafo.addVertex(20, "u");
		grafo.addVertex(21, "v");
		grafo.addVertex(22, "w");
		grafo.addVertex(23, "x");
		grafo.addVertex(24, "y");
		grafo.addVertex(25, "z");
		
		grafo.addEdge(18, 24, 18.24);
		grafo.addEdge(16, 24, 16.24);
		
		grafo.addEdge(0, 25, 0.25);
		grafo.addEdge(14, 25, 14.25);
		grafo.addEdge(11, 25, 11.25);
		
		grafo.addEdge(13, 19, 13.19);
		grafo.addEdge(10, 13, 10.13);
		grafo.addEdge(10, 21, 10.21);
		grafo.addEdge(13, 21, 13.21);
		grafo.addEdge(21, 23, 21.23);
		grafo.addEdge(6, 21, 6.21);
		grafo.addEdge(6, 20, 6.20);
		grafo.addEdge(2, 20, 2.20);
		grafo.addEdge(2, 10, 2.10);
		
		grafo.addEdge(1, 3, 1.3);
		grafo.addEdge(3, 8, 3.8);
		grafo.addEdge(3, 9, 3.9);
		grafo.addEdge(3, 4, 3.4);
		
		grafo.addEdge(12, 15, 12.15);
		grafo.addEdge(5, 12, 5.12);
		grafo.addEdge(7, 12, 7.12);
		grafo.addEdge(7, 17, 7.17);
		grafo.addEdge(5, 17, 5.17);
		
		java.util.Iterator<Integer> iter = grafo.adj(22);
		assertNull(iter);
		
		iter = grafo.adj(0);
		assertEquals((Integer)25, (Integer)iter.next());
		assertFalse(iter.hasNext());
		
		iter = grafo.adj(25);
		assertEquals((Integer)0, (Integer)iter.next());
		assertEquals((Integer)14, (Integer)iter.next());
		assertEquals((Integer)11, (Integer)iter.next());
		assertFalse(iter.hasNext());
		
		iter = grafo.adj(3);
		assertEquals((Integer)1, (Integer)iter.next());
		assertEquals((Integer)8, (Integer)iter.next());
		assertEquals((Integer)9, (Integer)iter.next());
		assertEquals((Integer)4, (Integer)iter.next());
		assertFalse(iter.hasNext());
		
		grafo = null;
	}
	
	public void testDfsYCCYGetCCYUncheck()
	{
		GrafoNoDirigido<Integer, String> grafo = new GrafoNoDirigido<>(0);
		
		
		grafo.addVertex(0, "a");
		grafo.addVertex(1, "b");
		grafo.addVertex(2, "c");
		grafo.addVertex(3, "d");
		grafo.addVertex(4, "e");
		grafo.addVertex(5, "f");
		grafo.addVertex(6, "g");
		grafo.addVertex(7, "h");
		grafo.addVertex(8, "i");
		grafo.addVertex(9, "j");
		grafo.addVertex(10, "k");
		grafo.addVertex(11, "l");
		grafo.addVertex(12, "m");
		grafo.addVertex(13, "n");
		grafo.addVertex(14, "o");
		grafo.addVertex(15, "p");
		grafo.addVertex(16, "q");
		grafo.addVertex(17, "r");
		grafo.addVertex(18, "s");
		grafo.addVertex(19, "t");
		grafo.addVertex(20, "u");
		grafo.addVertex(21, "v");
		grafo.addVertex(22, "w");
		grafo.addVertex(23, "x");
		grafo.addVertex(24, "y");
		grafo.addVertex(25, "z");
		
		grafo.addEdge(18, 24, 18.24);
		grafo.addEdge(16, 24, 16.24);
		
		grafo.addEdge(0, 25, 0.25);
		grafo.addEdge(14, 25, 14.25);
		grafo.addEdge(11, 25, 11.25);
		
		grafo.addEdge(13, 19, 13.19);
		grafo.addEdge(10, 13, 10.13);
		grafo.addEdge(10, 21, 10.21);
		grafo.addEdge(13, 21, 13.21);
		grafo.addEdge(21, 23, 21.23);
		grafo.addEdge(6, 21, 6.21);
		grafo.addEdge(6, 20, 6.20);
		grafo.addEdge(2, 20, 2.20);
		grafo.addEdge(2, 10, 2.10);
		
		grafo.addEdge(1, 3, 1.3);
		grafo.addEdge(3, 8, 3.8);
		grafo.addEdge(3, 9, 3.9);
		grafo.addEdge(3, 4, 3.4);
		
		grafo.addEdge(12, 15, 12.15);
		grafo.addEdge(5, 12, 5.12);
		grafo.addEdge(7, 12, 7.12);
		grafo.addEdge(7, 17, 7.17);
		grafo.addEdge(5, 17, 5.17);
		
		grafo.dfs(24);
		RedBlackBST<Integer, Vertex<Integer, String>> vs = grafo.darVertices();
		assertTrue(vs.get(24).isMarked());
		assertTrue(vs.get(18).isMarked());
		assertTrue(vs.get(16).isMarked());
		assertFalse(vs.get(22).isMarked());
		assertFalse(vs.get(9).isMarked());
		
		grafo.uncheck();
		assertFalse(vs.get(24).isMarked());
		assertFalse(vs.get(18).isMarked());
		assertFalse(vs.get(16).isMarked());
		
		assertEquals(6, grafo.cc());
		
		Iterator<Integer> iter = grafo.getCC(24);
		assertEquals((Integer)16, (Integer)iter.next());
		assertEquals((Integer)18, (Integer)iter.next());
		assertEquals((Integer)24, (Integer)iter.next());
		assertFalse(iter.hasNext());
	}
}
